library sidebar;

import 'package:cep_website/page/page.dart';
import 'package:flutter/material.dart';

part 'landscape.dart';

part 'portrait.dart';

part 'item.dart';

class Sidebar extends StatelessWidget {
  final Orientation orientation;

  const Sidebar(this.orientation, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Container(
        color: Colors.amber,
        child: orientation == Orientation.landscape
            ? const LandscapeSidebar()
            : const PortraitSidebar(),
      );
}
