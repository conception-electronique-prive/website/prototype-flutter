part of sidebar;

class SidebarItem extends StatelessWidget {
  final MainRoute route;
  final IconData icon;
  final String text;
  final bool expanded;

  const SidebarItem({
    Key? key,
    required this.route,
    required this.icon,
    required this.text,
    required this.expanded,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => InkWell(
        onTap: () => PageManager.mainRoute = route,
        child: Padding(
          padding: const EdgeInsets.only(
            left: 10,
            right: 10,
            top: 4,
          ),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 5),
                child: Icon(icon),
              ),
              if (expanded) Text(text),
            ],
          ),
        ),
      );
}
