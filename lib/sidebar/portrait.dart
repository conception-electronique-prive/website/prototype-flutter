part of sidebar;

class PortraitSidebar extends StatelessWidget {
  const PortraitSidebar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Row(
        children: [
          InkWell(
            onTap: () => Scaffold.of(context).openDrawer(),
            child: const Padding(
              padding: EdgeInsets.all(10),
              child: Icon(Icons.menu),
            ),
          ),
          const Expanded(
            child: Icon(Icons.adb),
          )
        ],
      );
}
