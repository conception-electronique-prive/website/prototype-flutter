part of sidebar;

class LandscapeSidebar extends StatelessWidget {
  const LandscapeSidebar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Column(
    crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            onTap: () {},
            child: const Padding(
              padding: EdgeInsets.all(10),
              child: Icon(Icons.menu),
            ),
          ),
          const SidebarItem(
            route: MainRoute.home,
            icon: Icons.home,
            text: "Home",
            expanded: true,
          ),
          const SidebarItem(
            route: MainRoute.services,
            icon: Icons.build,
            text: "Services",
            expanded: true,
          ),
          const SidebarItem(
            route: MainRoute.company,
            icon: Icons.business,
            text: "Company",
            expanded: true,
          ),
        ],
      );
}
