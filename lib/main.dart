import 'package:cep_website/app.dart';
import 'package:cep_website/page/page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MultiProvider(
    providers: [
      StreamProvider(
        create: (_) => PageManager.mainRouteStream,
        initialData: MainRoute.home,
      ),
    ],
    child: const WebsiteApp(),
  ));
}
