part of page;

class PageManager {
  static final PageManager _instance = PageManager._();

  PageManager._();

  final StreamController<MainRoute> _sMainRoute = StreamController();

  static Stream<MainRoute> get mainRouteStream => _instance._sMainRoute.stream;

  static set mainRoute(MainRoute route) {
    _instance._sMainRoute.add(route);
  }
}
