part of page;

class PageWrapper extends StatelessWidget {
  final Orientation orientation;

  const PageWrapper({Key? key, required this.orientation}) : super(key: key);

  Widget getContent(MainRoute route) {
    switch (route) {
      case MainRoute.home:
        return const ContentHome();
      case MainRoute.services:
        return const ContentServices();
      case MainRoute.company:
        return const ContentCompany();
      default:
        return Container();
    }
  }

  Offset getSlideBegin(Orientation orientation) =>
      orientation == Orientation.landscape
          ? const Offset(2.0, 0.0)
          : const Offset(0.0, 2.0);

  @override
  Widget build(BuildContext context) => Consumer<MainRoute>(
        builder: (context, route, __) => AnimatedSwitcher(
          duration: const Duration(milliseconds: 500),
          child: getContent(route),
          switchOutCurve: const Interval(0, 0),
          switchInCurve: Curves.ease,
          transitionBuilder: (child, animation) => SlideTransition(
            position: Tween<Offset>(
              begin: getSlideBegin(orientation),
              end: Offset.zero,
            ).animate(animation),
            child: child,
          ),
        ),
      );
}
