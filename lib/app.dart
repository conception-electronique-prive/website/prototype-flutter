import 'package:cep_website/page/page.dart';
import 'package:cep_website/sidebar/sidebar.dart';
import 'package:flutter/material.dart';

class WebsiteApp extends StatelessWidget {
  const WebsiteApp({Key? key}) : super(key: key);

  void onTapDrawer(BuildContext context, MainRoute route) {
    PageManager.mainRoute = route;
    Scaffold.of(context).closeDrawer();
  }

  @override
  Widget build(BuildContext context) => OrientationBuilder(
        builder: (context, orientation) => MaterialApp(
          home: Scaffold(
            drawer: Drawer(
              child: Builder(
                builder: (context) => ListView(
                  children: [
                    DrawerHeader(child: Icon(Icons.adb)),
                    ListTile(
                      onTap: () => onTapDrawer(context, MainRoute.home),
                      title: Text("Home"),
                    ),
                    ListTile(
                      onTap: () => onTapDrawer(context, MainRoute.services),
                      title: Text("Services"),
                    ),
                    ListTile(
                      onTap: () => onTapDrawer(context, MainRoute.company),
                      title: Text("Company"),
                    ),
                  ],
                ),
              ),
            ),
            body: Builder(
              builder: (context) => Flex(
                direction: orientation == Orientation.portrait
                    ? Axis.vertical
                    : Axis.horizontal,
                children: [
                  Sidebar(orientation),
                  Expanded(child: PageWrapper(orientation: orientation)),
                ],
              ),
            ),
          ),
        ),
      );
}
